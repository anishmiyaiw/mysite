from django.contrib import admin
from django.urls import path, include

from teacher import views

urlpatterns = [
    path('home', views.index)
]
