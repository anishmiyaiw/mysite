from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render
import matplotlib.pyplot as plt
import io
import urllib, base64
from student.models import Student


def index(request):
    data = Student.objects.all()
    context = {
        'students' : data
    }
    return render(request, 'student/home.html', context)


# Create your views here.
def mat_plot(request):
    plt.plot(range(10))
    fig = plt.gcf()
    # convert graph into dtring buffer and then we convert 64 bit code into image
    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)
    return render(request, 'home.html', {'data': uri})
